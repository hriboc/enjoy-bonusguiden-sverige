package se.devstudio.enjoybonusguidensverige;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import se.devstudio.enjoybonusguidensverige.model.Coupon;
import se.devstudio.enjoybonusguidensverige.model.CouponManager;

public class SplashActivity extends AppCompatActivity {

    private static final String TAG = SplashActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        // Connect to the database
        FirebaseDatabase db = FirebaseDatabase.getInstance();
        DatabaseReference dbRef = db.getReference("Coupons");

        // Read from the database
        // This method is called once with the initial value. Use .addValueEventListener()
        // instead to receive calls when firebase is updated.
        dbRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dinings) {
                List<Coupon> coupons = new ArrayList<>();

                for (DataSnapshot dining : dinings.getChildren()) {
                    Coupon coupon = dining.getValue(Coupon.class);

                    coupons.add(coupon);
                }
                CouponManager.getInstance().setCoupons(coupons);

                // öppna main activity
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                finish();
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
                Toast.makeText(getApplicationContext(), "Couldn't retrieve data from database", Toast.LENGTH_SHORT)
                        .show();
            }
        });
    }
}
