package se.devstudio.enjoybonusguidensverige;

import android.Manifest.permission;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import se.devstudio.enjoybonusguidensverige.model.Coupon;
import se.devstudio.enjoybonusguidensverige.model.CouponManager;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap map)
    {
        // Show blue dot of current location if permission is granted
        if (ActivityCompat.checkSelfPermission(this, permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            map.setMyLocationEnabled(true);
        }

        // add all markers
        List<Coupon> coupons = CouponManager.getInstance().getCoupons();
        for (Coupon c : coupons) {
            LatLng location = getLocationFromAddress(c.getStreet());

            if(location != null)
                map.addMarker(new MarkerOptions()
                        .position(location)
                        .title(c.getName())
                        .snippet(c.getCategory())
                        .icon(BitmapDescriptorFactory.defaultMarker(getMarkerColor(c))) //set color
                );
        }

        //  move the camera to gothenburg and zoom in
        LatLng got = new LatLng( 57.708870, 11.974560);
        map.moveCamera(CameraUpdateFactory.newLatLng(got));
        map.setMinZoomPreference(11);
    }

    private LatLng getLocationFromAddress(String strAddress)
    {
        Geocoder coder= new Geocoder(this);
        List<Address> addresses;
        LatLng location = null;

        try
        {
            addresses = coder.getFromLocationName(strAddress, 1);
            if(addresses==null)
                return null;

            Address address = addresses.get(0);
            location = new LatLng(address.getLatitude(), address.getLongitude());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return location;
    }

    private float getMarkerColor(Coupon c)
    {
        float color;

        switch (c.getCategory().toLowerCase()) {
            case "fint&festligt":
                color = BitmapDescriptorFactory.HUE_BLUE;
                break;
            case "gott&trevligt":
                color = BitmapDescriptorFactory.HUE_GREEN;
                break;
            case "snabbt&smakligt":
                color = BitmapDescriptorFactory.HUE_RED;
                break;
            case "café&bistro":
                color = BitmapDescriptorFactory.HUE_YELLOW;
                break;
            default:
                color = BitmapDescriptorFactory.HUE_CYAN;
                break;
        }
        return color;
    }
}
