package se.devstudio.enjoybonusguidensverige;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;

public class MainActivity extends AppCompatActivity {

    private ImageButton imgButDining;
    private ProgressBar pbLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // init UI components
        pbLoading = findViewById(R.id.pb_loading);
        imgButDining = findViewById(R.id.imgbtn_dining);

        imgButDining.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pbLoading.setVisibility(View.VISIBLE);
                startActivity(new Intent(MainActivity.this, MapsActivity.class));
            }
        });
    }

    /**
     * Dispatch onStop() to fragments.
     */
    @Override
    protected void onStop() {
        super.onStop();

        pbLoading.setVisibility(View.GONE);
    }
}
