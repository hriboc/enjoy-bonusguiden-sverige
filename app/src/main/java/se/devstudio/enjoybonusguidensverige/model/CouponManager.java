package se.devstudio.enjoybonusguidensverige.model;

import java.util.ArrayList;
import java.util.List;

public class CouponManager {

    private List<Coupon> coupons;
    private static final CouponManager ourInstance = new CouponManager();

    public static CouponManager getInstance() {
        return ourInstance;
    }

    private CouponManager() {
        coupons = new ArrayList<>();
    }

    public void setCoupons(List<Coupon> coupons) {
        this.coupons = coupons;
    }

    public List<Coupon> getCoupons() {
        return coupons;
    }
}
